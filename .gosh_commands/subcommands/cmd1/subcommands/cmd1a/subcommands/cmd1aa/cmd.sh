#!/bin/bash

function get_help () {
  echo "This is the help text for cmd1aa"
}
export -f get_help

function get_description () {
  echo "cmd1aa is a sample command.  It doesn't do much."
}
export get_description

execute_cmd () {
  echo "cmd1aa has executed!"
}
export execute_cmd
