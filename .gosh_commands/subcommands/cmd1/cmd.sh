#!/bin/bash

function execute_cmd () {
  echo "cmd1 has executed!"
}
export execute_cmd

function get_help () {
  echo "This is help content from cmd1."
  echo "Arguments:"
  for A in "$@";
  do
    printf "\t* $A\n"
  done
}
export get_help

function get_description () {
  echo "This is the description of this command."
}
export get_description
