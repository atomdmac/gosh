#!/bin/bash

function get_help () {
  echo "This is the help text for cmd2"
}
export get_help

function get_description () {
  echo "cmd2 is a sample command.  It doesn't do much."
}
export get_description
