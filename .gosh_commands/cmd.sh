#!/bin/bash

function get_help () {
  echo "The gosh command is meant to help developers organize their utility scripts and tools."
}
export get_help

function get_description () {
  echo "A utility script manager"
}
export get_description
