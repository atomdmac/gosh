#!/bin/bash

# The user-facing entrypoint into gosh.

source "$GOSH_ROOT/gosh-fns.sh"

# If data is piped to this script, pull input from stdin.
# Else pull from arguments
if [[ -p /dev/stdin ]]; then
  read -r INPUT
else
  INPUT=$@
fi

echo ""
if [ -z $GOSH_DISABLE_BANNER ]; then print_banner; fi

use_cmd_execute $INPUT
unload_cmd

echo ""
