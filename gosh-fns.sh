#!/bin/bash

# Reusable functions for powering go.sh

# Colors
black="\e[30m"
red="\e[31m"
green="\e[32m"
brown="\e[33m"
blue="\e[34m"
purple="\e[35m"
cyan="\e[36m"
gray="\e[37m"
end="\e[0m"

# Formatting
bold="\e[1m"
underline="\e[3m"

# Translate a subcommands full name to a filesystem path.
function get_cmd_path () {
  CMD_PATH=$GOSH_COMMANDS_ROOT
  for P in "$@"
  do
    if [[ -d "${CMD_PATH}/subcommands/${P}" ]]; then
      CMD_PATH="${CMD_PATH}/subcommands/${P}"
    fi
  done
  echo "$CMD_PATH"
}

# Determine how deeply nested a given subcommand is.
function get_cmd_depth () {
  CMD_PATH=$GOSH_COMMANDS_ROOT
  SUBCMD_COUNT=0
  for P in "$@"
  do
    if [[ -d "${CMD_PATH}/subcommands/${P}" ]]; then
      CMD_PATH="${CMD_PATH}/subcommands/${P}"
      SUBCMD_COUNT=$((SUBCMD_COUNT + 1))
    fi
  done
  echo $SUBCMD_COUNT
}

# Return a list of subcommands (if any) given the full name of a subcommand.
function get_subcommands () {
  CMD_PATH=$(get_cmd_path "$@")
  for F in $CMD_PATH/subcommands/*;
  do
    if [[ -d "$F" ]]; then
      basename "$F"
    fi
  done
}

# Determine whether the given name refers to a shell function.
function is_function () {
  if  [[ "$(type "$1" 2>/dev/null | head -1)" == "$1 is a function" ]]; then
    echo 0
  else
    echo 1
  fi
}

# Execute a subcommand given it's full name.
function use_cmd_execute () {
  source_cmd "$@"

  HAS_EXEC=$(is_function "execute_cmd")

  if [[ "$HAS_EXEC" -eq 0 ]]; then
    shift "$(get_cmd_depth "$@")"

    # If the -h flag is the last argument given, show the help for the given
    # command instead of executing it.
    for LAST; do true; done
    if [[ $LAST == "-h" ]]; then
      get_cmd_help "$@"
    else
      execute_cmd "$@" || get_cmd_help "$@"
    fi
  else
    get_cmd_help "$@"
  fi

  unset use_cmd
}

function unload_cmd () {
  unset execute_cmd
  unset get_help
  unset get_description
}

# Source the shell script for a subcommand given it's full name.
function source_cmd () {
  CMD_PATH=$(get_cmd_path $@)

  if [[ -f "$CMD_PATH/cmd.sh" ]]; then
    source "$CMD_PATH/cmd.sh"
  fi
}

# Display all available documentation for a given subcommand.
function get_cmd_help () {
  for CMD_NAME; do true; done
  source_cmd "$@"
  HAS_HELP=$(is_function "get_help")
  HAS_DESC=$(is_function "get_description")

  printf "\n${cyan}${bold}# NAME:${end}\n"
  printf "${CMD_NAME}\n"
  printf "\n${cyan}${bold}# DESCRIPTION:${end}\n"
  if [[ "$HAS_DESC" -eq 0 ]]; then
    get_description "$@"
  else
    echo "-"
  fi

  SUBCOMMANDS=$(get_subcommands "$@")
  if [[ -n "$SUBCOMMANDS" ]]; then
    printf "\n${cyan}${bold}# SUB-COMMANDS:${end}\n"
    echo "$SUBCOMMANDS"
  fi

  printf "${cyan}\n${bold}# HELP:${end}\n"
  if [[ "$HAS_HELP" -eq 0 ]]; then
    get_help "$@"
  else
    echo "-"
  fi
}

# Create necessary directory structures and shell scripts for a new subcommand.
function scaffold_cmd () {
  if ! [[ -a "$GOSH_COMMANDS_ROOT" ]]; then
    mkdir "$GOSH_COMMANDS_ROOT"
  fi

  CMD_PATH="$GOSH_COMMANDS_ROOT"
  for P in "$@"
  do
    CMD_PATH="${CMD_PATH}/subcommands/${P}"
    if ! [[ -a "$CMD_PATH" ]]; then
      mkdir -p "$CMD_PATH"
      touch "$CMD_PATH/cmd.sh"
      chmod u+x "$CMD_PATH/cmd.sh"
    fi
  done
}

function print_banner () {
  printf "${blue}${bold}"
  printf "   __________   _____ __  __\n"
  printf "  / ____/ __ \ / ___// / / /\n"
  printf " / / __/ / / / \__ \/ /_/ / \n"
  printf "/ /_/ / /_/ / ___/ / __  /  \n"
  printf "\____/\\____(_)____/_/ /_/  \n"
  printf "${end}\n"
}
