# A Framework for "Go" Scripts

![An example of gosh in use](./doc/usage.gif)

`Gosh` is a way to quickly define, document, discover and execute the tools and scripts that make your life as a developer easier.  Build processes, test scripts, directory grooming scripts, etc. are all examples of things that might live behind `gosh`.  It's basically some search/auto-complete on top of a directory tree with shell scripts in it, plus a little extra structure to take some of the thinking out of it.

# Installation

1. Clone this repository to your computer somewhere
2. Add the cloned repository to your `PATH`
3. Add your utility scripts to a `.gosh_commands` directory inside of your project directory (see below).
4. Invoke your commands like this: `gosh [subcommand] [...subcommand] [-h]`
5. For ZSH auto-complete, copy `_gosh` to any directory listed under `echo $FPATH`

# Usage

```sh
gosh [subcommand] [...subcommand] [-h]
```

When the `gosh` command is invoked, `gosh` will look for a `.gosh_commands` directory in the current working directory.  This is where all of your subcommands will live.  Subcommands are the tools that make your life as a developer easier.  Anything that you find yourself doing repeatedly could potentially become a subcommand.

## Adding a New Subcommand

To create the skeleton for a new subcommand, use the `+add` built-in command like so:

```bash
gosh +add my cool tool
```

This will create a new tree of subcommands where `my` is the top-level subcommand and `tool` is the most deeply nested one.  The directory structure ends up looking like this:

```
.gosh_commands/
|- subcommands/
|  |- my/
|  |  |- cmd.sh
|  |  |- subcommands/
|  |  |  |- cool/
|  |  |  |  |- cmd.sh
|  |  |  |  |- subcommands/
|  |  |  |  |  |- tool/
|  |  |  |  |  |  |- cmd.sh
```

At each level, there's a `cmd.sh` file that represents a tool that can be used.  Since they're just shell scripts, they can do anything you'd manually do from your terminal (ex. copy files, clean folders, kick off Docker builds, push code, start other, non-shell processes, etc.)

## Anatomy of a `cmd.sh` File

A `cmd.sh` script is a normal `bash` script that can optionally define a number of functions that allow `gosh` to interact with it.

| Function          | Purpose                                                                               |
|-------------------|---------------------------------------------------------------------------------------|
| `execute_cmd`     | Called when the subcommand is invoked.  This is what makes your subcommand Do Stuff ™️ |
| `get_help`        | Defines help text for the subcommand.  Put your "usage" information here.             |
| `get_description` | Provides a description of the subcommand                                              |

## FZF Integration

To make finding subcommands as easy as possible, `gosh` allows you to use FZF for fuzzy-finding and previewing by default.  This can be disabled by setting the `GOSH_DISABLE_FZF` variable to any non-empty value.  If FZF is not installed, `gosh` falls back to shell-based auto-complete (if supported).

# FAQ

## Why Bash?

Bash and bash-compatible shells are available everywhere I like to work and scripts are fairly straight-forward to write.  If you prefer _not_ to write shell scripts, you can just as easily launch tools written in your language of choice from your `cmd.sh` scripts.

## But Bash scripts are awful to write

Fair enough.  Feel free to rewrite this project in Rust for a more Blazingly Fast ™️  experience.

## Wait, this isn't Golang...

You are correct :) The term "go script" is one that I've heard a lot in my professional life so that's what I used here.  I suppose is refers to to the fact that the script is the central entrypoint if you want to "go get things done".  Plus, it's super fast to type.
